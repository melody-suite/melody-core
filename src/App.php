<?php

namespace Orchestra;

use Orchestra\Helpers\Arr\Arr;
use Orchestra\Pipeline\Pipeline;
use Orchestra\Singleton\Contracts\Singleton;
use Orchestra\Singleton\Traits\Singleton as TraitsSingleton;

class App implements Singleton
{
   use TraitsSingleton;

   private $requestMiddlewaresResult;

   private $responseMiddlewaresResult;

   public function bootstrap()
   {
      $this->runRequestPipeline();

      $this->runResponsePipeline();

      return Arr::get($this->responseMiddlewaresResult, "response_result");
   }

   private function runRequestPipeline()
   {
      $this->requestMiddlewaresResult = Pipeline::send([])
         ->through(requestMiddlewares())
         ->run()
         ->getReturn();
   }

   private function runResponsePipeline()
   {
      $this->responseMiddlewaresResult = Pipeline::send($this->requestMiddlewaresResult)
         ->through(responseMiddlewares())
         ->run()
         ->getReturn();
   }
}

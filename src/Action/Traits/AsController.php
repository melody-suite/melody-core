<?php

namespace Orchestra\Action\Traits;

use Orchestra\Exceptions\RequestException;
use Orchestra\Exceptions\UnauthorizedAction;
use Orchestra\Exceptions\ValidationException;
use Orchestra\Validation\Validation;

trait AsController
{
   use Action;

   protected $validator;

   protected $validated;

   public function authorize(): bool
   {
      return true;
   }

   public function beforeValidation()
   {
   }

   public function validate()
   {
      return [];
   }

   public function afterValidation()
   {
   }

   public function __invoke($attributes = [])
   {
      $this->attributes = empty($this->attributes) ? $attributes : array_merge_recursive($this->attributes, $attributes);

      if (!$this->authorize()) {
         throw new UnauthorizedAction("Unauthorized action called", 401);
      }

      $this->runValidation();

      return $this->handle();
   }

   private function runValidation()
   {
      $rules = $this->validate();

      if (!empty($rules)) {
         try {
            $this->validator = new Validation();

            $this->validator->run($rules, $this->attributes);

            $this->validated = $this->validator->getValidated();
         } catch (ValidationException $e) {
            throw new RequestException(serialize($this->validator->getErrors()), 422);
         }
      }
   }
}

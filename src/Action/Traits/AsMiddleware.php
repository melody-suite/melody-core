<?php

namespace Orchestra\Action\Traits;

use Orchestra\Pipeline\Traits\Pipe;

trait AsMiddleware
{
   use Action,
      Pipe;
}

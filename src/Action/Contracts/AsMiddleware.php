<?php

namespace Orchestra\Action\Contracts;

use Orchestra\Pipeline\Contracts\Pipe;

interface AsMiddleware extends Pipe
{
   public function createFrom($class): object;
}

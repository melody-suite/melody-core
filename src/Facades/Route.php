<?php

namespace Orchestra\Facades;

use Orchestra\Facade\Facade;
use Orchestra\Router\Router;

class Route extends Facade
{
   public static function concrete(): string
   {
      return Router::class;
   }
}

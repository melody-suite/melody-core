<?php

namespace Orchestra\Middlewares;

use Orchestra\Action\Contracts\AsMiddleware;
use Orchestra\Action\Traits\AsMiddleware as TraitsAsMiddleware;
use Orchestra\Helpers\Arr\Arr;
use Orchestra\Router\RouterCollection;

class ResolveRoute implements AsMiddleware
{
   use TraitsAsMiddleware;

   public function handle(&$data)
   {
      Arr::set($data, "request_result", RouterCollection::resolve($_SERVER["REQUEST_URI"], $_SERVER['REQUEST_METHOD']));

      return $this->next($data);
   }
}

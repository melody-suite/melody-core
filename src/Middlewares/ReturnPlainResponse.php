<?php

namespace Orchestra\Middlewares;

use Orchestra\Action\Contracts\AsMiddleware;
use Orchestra\Action\Traits\AsMiddleware as TraitsAsMiddleware;
use Orchestra\Helpers\Arr\Arr;

class ReturnPlainResponse implements AsMiddleware
{
   use TraitsAsMiddleware;

   public function handle(&$data)
   {
      return Arr::set($data, "response_result", $data["request_result"]);
   }
}

<?php

use Orchestra\App;
use Orchestra\Exceptions\RequestException;

function run()
{
   try {
      echo App::getInstance()->bootstrap();
   } catch (RequestException $e) {
      header(":", true, $e->getCode());

      exit;
   }
}
